﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutisticControls : MonoBehaviour {
    private CharacterController AutisticController;
    public bool isgrounded;
    public float gravity;
    private float fallspeed;
    public float jumpspeed;
    public float movespeed;
    public float dashboost;
    public float energy=6;
    /*public float coolDownPeriodInSeconds=5f;
    public float timeStamp=0;*/
	// Use this for initialization
	void Start () {
        AutisticController = GetComponent<CharacterController>  ();
	}
	
	// Update is called once per frame
	void Update () {
       
        Isgrounded();
        Fall();
        jump();
        move(); 
        dash();
        energyres();

	}
   /* bool cooldown()
    {

        if (timeStamp == coolDownPeriodInSeconds)
        {
            timeStamp = 0;
            return true;
        }
        else
        {
            while (timeStamp < coolDownPeriodInSeconds)
                timeStamp = timeStamp + Time.deltaTime;
            return false;
        }
    }*/
   void move()
    {
        float xspeed = Input.GetAxis("Horizontal");
        if (xspeed != 0) AutisticController.Move(new Vector3(xspeed, 0) * movespeed * Time.deltaTime);
    }
    void jump()
    {
        if(Input.GetButtonDown("Jump")&& isgrounded)
        {
            fallspeed = -jumpspeed;
        }
    }
    void dash()
    {
       

        float xspeed = Input.GetAxis("Horizontal");
            if (Input.GetButtonDown("Fire1") && energy > 5)
            {

                AutisticController.Move(new Vector3(xspeed, 0) * movespeed * dashboost * Time.deltaTime*25);
                energy = energy - 500 * (Time.deltaTime);
            }
        
    }
   void energyres()
       {
       if(energy<6)
        energy = energy + 5 * Time.deltaTime;

        
        }
    void Fall() {
        if(!isgrounded)
            {
            fallspeed += gravity * Time.deltaTime;
            }
        else
        {
            if (fallspeed > 0) fallspeed = 0;

        }
        AutisticController.Move(new Vector3(0, -fallspeed) * Time.deltaTime);
    }
    void death()
    {
        if(gameObject.transform.position.y<-15F)
        {
            print("RIP");
            
        }
    }
    void Isgrounded() {
        isgrounded= (Physics.Raycast(transform.position, -transform.up, AutisticController.height / 1.57F)) ;
    }
}
