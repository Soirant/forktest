﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterController : MonoBehaviour
{

    public float speed = 10.0F;
    public float jumpvel = 25;
    //public float distToGr = 0.4f;
    //public bool IsGrounded;
    //public LayerMask ground;
    /*}
    [System.Serializable]
    public class PhysSettings
    
    {
        public float downAccel = 0.75F;
    }
    [System.Serializable]
    public class InputSettings
    {
        public string FORWARD_AXIS = "Vertical";
        public string STRAFFE_AXIS = "Horizontal";
        public string JUMP_AXIS = "Jump";
    }

    public MoveSettings moveSetting = new MoveSettings();
    public PhysSettings physSetting = new PhysSettings();
    public InputSettings inputSetting = new InputSettings();
     Vector3 velocity = Vector3.zero;
    Rigidbody rBody;
   public float jumpInput;
   public float translation;
   public float straffe;*/



    // Use this for initialization
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        /*  jumpInput=0;
         translation=0;
         straffe=0*/
        ;
    }
    /*void GetInput()
    {
        translation = Input.GetAxis(inputSetting.FORWARD_AXIS);
        straffe = Input.GetAxis(inputSetting.STRAFFE_AXIS);
        jumpInput = Input.GetAxisRaw(inputSetting.JUMP_AXIS);
    }*/
    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown("escape"))
            Cursor.lockState = CursorLockMode.None;
    }
    void FixedUpdate()
    {
        move();

    }
    void move()
    {

        float translation = Input.GetAxis("Vertical") * speed;
        float straffe = Input.GetAxis("Horizontal") * speed;
        translation *= Time.deltaTime;
        straffe *= Time.deltaTime;
        transform.Translate(straffe, 0, translation);
    }
}
/*ol grounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, moveSetting.distToGr, moveSetting.ground);
    }
void jump()
    {
        jumpInput = Input.GetAxisRaw("Jump");
        if(jumpInput > 0 && grounded())
        {
            velocity.y = moveSetting.jumpvel;
        }
        else  if (jumpInput==0 && grounded())
        {
            velocity.y = 0;
        }
        else
        {
            velocity.y -= physSetting.downAccel;

        }
        
    }
}
*/